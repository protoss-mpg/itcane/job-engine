package com.mpg.itcane.job.task;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.protosstechnology.commons.util.DateUtil;

import lombok.extern.slf4j.Slf4j;


@Slf4j
@Component
public class CheckActiveTask {
	
	@Autowired
    RestTemplate restTemplate;
	
	@Scheduled(cron = "* * * * * *")
    public void reportCurrentTime() {
    	log.info("***************************************");
    	log.info("The time is now {}" , DateUtil.getCurrentDateTimestamp());
    	log.info("***************************************");
    }

}
