/**
 * 
 */
/**
 * @author suriya_e@protosstechnology.com
 *
 */
package com.mpg.itcane.job.configuration.swagger;


import java.util.ArrayList;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.google.common.base.Predicates;

import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {
    @Bean
    public Docket productApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.any())
                .paths(Predicates.not(PathSelectors.regex("/error.*")))
                .build()
                .apiInfo(metaData());
    }
    
    private ApiInfo metaData() {
        return  new ApiInfo(
        		"Job REST API",
        		"Job Engine REST API for Mitr Phol",
        		"1.0",
        		"Terms of service",
        		new Contact("ITCane", "http://www.mitrphol.coom", "contact@mitrphol.com"),
        		"Apache License Version 2.0",
        		"https://www.apache.org/licenses/LICENSE-2.0",
        		new ArrayList());
    }
}


