package com.mpg.itcane.job.configuration.aop;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

import com.protosstechnology.commons.util.DateUtil;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
@Aspect
public class LoggingAspect {

    @Pointcut("@annotation(com.mpg.itcane.job.configuration.aop.annotation.LogAccess)")
    public void logAccessMethod() {
    	log.info("Define Point Cut");
    }

    @Around("logAccessMethod()")
    public Object profile(ProceedingJoinPoint pjp) throws Throwable {
            long start = System.currentTimeMillis();
            Object output = pjp.proceed();
            long elapsedTime = System.currentTimeMillis() - start;
            log.info(pjp.getSignature().getName()+"|"+DateUtil.getCurrentDateTimestamp()+"|"+elapsedTime);
           
            return output;
    }

}

