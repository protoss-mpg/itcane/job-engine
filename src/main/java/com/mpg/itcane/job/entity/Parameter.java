package com.mpg.itcane.job.entity;


import java.sql.Timestamp;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Version;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of={"id"})
public class Parameter {

	/* Standard Set of field */
    private  @Id @GeneratedValue(strategy = GenerationType.TABLE) Long id;
    private @Version @JsonIgnore Long version;
    private @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss") Timestamp createDate;
    private String createBy;
    private @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss") Timestamp updateDate;
    private String updateBy;
    private Boolean flagActive;

    private String code;
    private String name;
    private String description;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "parameter")
    private Set<ParameterDetail> details;


 

}