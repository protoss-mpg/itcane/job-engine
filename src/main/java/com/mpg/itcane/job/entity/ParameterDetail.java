/**
 * 
 */
/**
 * @author suriya_e@protosstechnology.com
 *
 */
package com.mpg.itcane.job.entity;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Version;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of = {"id"})
public class ParameterDetail {

	/* Standard Set of field */
    private  @Id @GeneratedValue(strategy = GenerationType.TABLE) Long id;
    private @Version @JsonIgnore Long version;
    private @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss") Timestamp createDate;
    private String createBy;
    private @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss") Timestamp updateDate;
    private String updateBy;
    private Boolean flagActive;
    
    private String code;
    private String name;
    private String description;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "parameter")
    private Parameter parameter;

    private String variable1;
    private String variable2;
    private String variable3;
    private String variable4;
    private String variable5;
    private String variable6;
    private String variable7;
    private String variable8;
    private String variable9;
    private String variable10;
    
}