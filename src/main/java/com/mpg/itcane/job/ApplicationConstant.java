package com.mpg.itcane.job;

public class ApplicationConstant {
	
	private ApplicationConstant() {
		super();
	}
	
	public static final String PARAMETER_JOB_CONFIGURE        = "JOB001";
	
	/* ERROR CODE */
	public static final String ERROR_CODE_PROCESS_FAIL        = "JOBERR001";
	public static final String ERROR_CODE_THROW_EXCEPTION     = "JOBERR002";
	public static final String ERROR_CODE_NO_DATA_FOUND       = "JOBERR003";
	public static final String ERROR_CODE_REQUIRE_DATA        = "JOBERR004";
}
