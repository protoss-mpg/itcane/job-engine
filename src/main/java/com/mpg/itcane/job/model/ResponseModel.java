/**
 * 
 */
/** 
 * @author suriya_e@protosstechnology.com
 *
 */
package com.mpg.itcane.job.model;


import java.util.HashMap;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.mpg.itcane.job.ApplicationConstant;


public class ResponseModel {
	
	
	private Boolean success;
	private String message;
	@JsonInclude(Include.NON_NULL)
	private String errorCode;
	private Object data;
	
	public ResponseModel() {
		setSuccess(false);
    	setErrorCode(ApplicationConstant.ERROR_CODE_PROCESS_FAIL);
        setMessage("Process Fail");
        setData(new HashMap());
	}

	public Boolean getSuccess() {
		return success;
	}

	public void setSuccess(Boolean success) {
		if(success){
			this.errorCode = null;
		}
		this.success = success;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}
	
	
	

}
