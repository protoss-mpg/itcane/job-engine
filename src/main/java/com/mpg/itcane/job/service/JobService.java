package com.mpg.itcane.job.service;

import com.mpg.itcane.job.model.ResponseModel;

public interface JobService {

	public ResponseModel executeJob(String jobCode);
	
	
}
